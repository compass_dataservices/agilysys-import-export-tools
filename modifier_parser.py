#! python

import logging
import os
import re
import sys
import argparse
import pandas as pd
from dataclasses import dataclass, field
from openpyxl import Workbook
from typing import ClassVar, FrozenSet, Tuple

PRICE_ARRAY_REGEX = re.compile(r'(?<=\{)[^(\{|\})].+?(?=\})')
QUOTED_COMMAS_REGEX = re.compile(r'((?<=")[^",\{\}]+),([^"\{\}]*(?="))')

logname = __name__ if __name__ != '__main__' else os.path.splitext(os.path.basename(__file__))[0]
logger = logging.getLogger(logname)
fh = logging.FileHandler('modifier_parser.log')
stream = logging.StreamHandler()
fmt = '%(asctime)s %(name)-12s %(levelname)-8s %(message)s'


@dataclass
class ModifierItem:
    action: str
    id: int
    name: str
    abbr1: str = field(default='', repr=False)
    abbr2: str = field(default='', repr=False)
    name2: str = field(default='', repr=False)
    name3: str = field(default='', repr=False)
    attr1: int = field(default=0, repr=False)
    attr2: int = field(default=0, repr=False)
    attr3: int = field(default=0, repr=False)
    attr4: int = field(default=0, repr=False)
    attr5: int = field(default=0, repr=False)
    attr6: int = field(default=0, repr=False)
    _price_levels: str = ''
    attr7: str = field(default='', repr=False)
    _price_update_fields: ClassVar[FrozenSet[str]] = frozenset(('action', 'id', 'price_string'))
    _repr_fields: ClassVar[Tuple[str]] = ('action', 'id', 'name', 'abbr1', 'abbr2', 'name2', 'name3', 'attr1', 'attr2', 'attr3', 'attr4', 'attr5', 'attr6', 'price_string', 'attr7')

    @property
    def price_levels(self):
        if isinstance(self._price_levels, str):
            prices = self._price_levels.strip('"{}').split(";")
            return {x.split('|')[0]:x.split('|')[1] for x in prices}
        elif isinstance(self._price_levels, dict):
            return self._price_levels
        else:
            raise AttributeError('Unable to parse price levels from %s' % self._price_levels)

    @property
    def price_string(self):
        return '"{%s}"' % ','.join(['{}|{:.2f}'.format(k, float(v)) for k, v in self.price_levels.items()])

    @property
    def full_update_string(self):
        return ','.join([str(getattr(self, attr)) for attr in self._repr_fields])

    @property
    def price_update_string(self):
        return ','.join([str(getattr(self, attr)) if attr in self._price_update_fields else '' for attr in self._repr_fields])


def count_price_levels(items):
    num_price_levels = 0
    price_level_set = set()
    for item in items:
        for level in item.price_levels.keys():
            if level not in price_level_set:
                price_level_set.add(level)
    return sorted(price_level_set, key=lambda x: int(x))


def replace_commas(match, replacement=None):
    replacement = replacement or ';'
    match = str(match.group(0))
    return match.replace(',', replacement)


def parse_file(file_name):
    logger.debug('parsing {}'.format(file_name))
    items = []
    with open(file_name) as f:
        for i, line in enumerate(f):
            item = line.strip('\n').strip(',')
            item = PRICE_ARRAY_REGEX.sub(replace_commas, item)
            item = QUOTED_COMMAS_REGEX.sub(replace_commas, item)
            try:
                items.append(ModifierItem(*item.split(',')))
            except TypeError:
                logger.warning(f'Unable to parse item: {i}. {item}')
                _ = input('Press Enter to continue...')
    return items


def convert_to_excel(file_name):
    logger.debug('converting {} to excel'.format(file_name))
    items = parse_file(file_name)
    price_levels = count_price_levels(items)
    price_levels_headers = ['Price Level %s' % p for p in price_levels]
    dest_filename = generate_dest_filename(file_name, 'xlsx')

    wb = Workbook()
    ws1 = wb.active
    headers = ['Action', 'ID', 'Name']
    headers.extend(price_levels_headers)
    ws1.append(headers)

    for item in items:
        row = ['U', item.id, item.name]
        price_cols = [item.price_levels.get(x) for x in price_levels]
        row.extend(price_cols)
        ws1.append(row)

    wb.save(dest_filename)


def generate_dest_filename(filename, extension):
    base_name = os.path.splitext(os.path.basename(filename))[0]
    dir_name = os.path.dirname(filename)
    dest_filename = os.path.join(dir_name, f'{base_name}.{extension}')
    return dest_filename


def parse_dataframe(df):
    items = []
    df = df[df['Action'] == 'U']
    df = df.rename(lambda x: x.replace(' ', '_'), axis='columns')
    rows = [x._asdict() for x in df.itertuples()]
    for row in rows:
        price_dict = {k.replace('Price_Level_', ''): str(v) for k, v in row.items() if 'price' in k.lower() and str(v) != 'nan'}
        items.append(ModifierItem(row['Action'], row['ID'], row['Name'], _price_levels=price_dict))
    return items


def create_import(filename):
    logger.debug('creating import from {}'.format(filename))
    df = pd.read_excel(filename)
    items = parse_dataframe(df)
    dest_filename = generate_dest_filename(filename, 'txt')
    with open(dest_filename, 'w') as f:
        for item in items:
            f.write(item.price_update_string + '\r\n')
    logger.info('processing of %s completed', filename)


def pause():
    if 'win' in sys.platform:
        os.system('pause')
    elif sys.platform == 'linux':
        os.system('/bin/bash -c \'read -s -n 1 -p "Press any key to continue..."\'')
        print()
    else:
        input(f'Unsupported platform: {sys.platform}')


def main(filename):
    logger.debug('processing %s', filename)
    ext = os.path.splitext(filename)[1]
    if ext == '.txt':
        logger.debug(f'converting {ext} to excel')
        convert_to_excel(filename)
    elif 'xls' in ext:
        logger.debug(f'converting {ext} to txt import')
        create_import(filename)
    else:
        raise ValueError(f'Unable to process {ext} extension')


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Convert Agilysys Modifier Export txt <-> xlsx')
    parser.add_argument('filename', type=str, help='the file to be converted')
    parser.add_argument('--logging', help='log level to use')
    args = parser.parse_args()
    try:
        log_level = getattr(logging, args.logging.upper())
    except AttributeError:
        log_level = logging.INFO
    logging.basicConfig(format=fmt, level=log_level, handlers=[fh, stream])

    try:
        logger.info('-' * 20)
        logger.debug(args)
        main(args.filename)
        logger.info('-' * 20)
    except:
        logger.exception('Something went wrong')
        pause()

